---
layout: default
---

Informació:
=====
Documentals:
-----
Títol | Autoria | Programa | Enllaç | Durada
--- | --- | ---
Comprar, tirar, comprar. La historia secreta de la obsolencia programada | Cosmina Dannoritzer | Los documentales de La 2 (sí, en sèrio, no és conya) | https://www.youtube.com/watch?v=ZTVOBBbnjv4 | 1H14M
Residuos electrónicos, cambio ambiental | Universidad Nacional Tres de Febrero | Encuentro | https://www.youtube.com/watch?v=dJ3N8KGKflc | 26:40m
Arduino: el documental | | | https://www.youtube.com/watch?v=zmbXmpvfVJ0 | 28m
Somos legión: la historia de los hackers | | | https://www.youtube.com/watch?v=HzAZT_wgqp0 | 1h35m
WikiRebels: el documental de WikiLeaks |  | SVT | https://www.youtube.com/watch?v=_jFmAp3unD8 | 57m
Anonimous Giberguerrilla'08 |  |  | https://www.youtube.com/watch?v=BhJfaKAycNs | 51:48m
La tragedia electrónica | Cosima Dannoritzer | Documentales de La 2 | https://www.youtube.com/watch?v=dvi0fKFYH00 | 33:43m
TPB AFK | Simon Klose |  | https://youtu.be/eTOKXCEwo_8 |
Virtual Revolution |  | BBC | https://www.youtube.com/watch?v=jYpPt9d971I |
Citizen Four |  |  | http://m.imdb.com/title/tt4044364/ | 114 min
The internet's own boy |  |  | http://m.imdb.com/title/tt3268458/ | 105 min
Blood in the mobile (Sang al mòbil) - Conflicte armat República Democràtica del Congo" | | | | |


Aportacions:
=====

guifipedro:
-----
[creative commons]
1:05 dibuixos de gràfic xarxa nodes, routers (castellà)
https://www.youtube.com/watch?v=0gq1BW02xUc
1:05 dibuixos de gràfic xarxa nodes, routers (alemany amb molts subtítols)
https://www.youtube.com/watch?v=2Z12OjnPADA

videos de guifi, que podeu utilitzar:
5:02 racks fibra
https://www.youtube.com/watch?v=gD3HYeD4Lm4
3:29 antenes racks fibra
https://www.youtube.com/watch?v=C3GpZWxOBoQ
8:00 antenes wifi
https://vimeo.com/85424312

mèxico - xarxa telefonia (crec que no hi haurà problema de reutilitzar)
10:11 torretes teleco, mòbils
https://www.youtube.com/watch?v=qQfwQ7sGvgM&feature=youtu.be

http://www.theguardian.com/technology/2008/sep/29/cloud.computing.richard.stallman
http://arstechnica.com/information-technology/2008/09/why-stallman-is-wrong-when-he-calls-cloud-computing-stupid/
http://www.theregister.co.uk/2011/03/31/google_on_open_source_licenses/


Simona Levi Xnet
-----
NEUTRALITAT DE LA XARXA
Neutralitat de la Xarxa  (Outcomes FCForum2015)
http://2015.fcforum.net/neutralidad-de-la-red

Internet VS el Gobierno español y Pilar del Castillo (neutralitat de la xarxa)
https://xnet-x.net/es/internet-vs-pilar-del-castillo/

Open letter to Mark Zuckerberg on Net Neutrality advocacy in India
https://www.accessnow.org/open-letter-to-mark-zuckerberg-on-net-neutrality-in-india/

Més info:
https://xnet-x.net/ca/eix/neutralitat-de-la-xarxa/


LLIBERTAT D'EXPRESSIÓ
Llibertat d'expressió i accés a la informació online (Outcomes FCForum2015)
http://2015.fcforum.net/libertad-de-expresion-y-derecho-a-la-informacion-vs-secretos-comerciales/

Legislacions que coarten la llibertat d'expressió, d'acció i d'organització a l'Estat espanyol
https://xnet-x.net/leyes-coartan-libertad-expresion-accion-organizacion


PRIVACITAT
Transparencia y Privacidad (Outcomes FCForum2015)
https://xnet-x.net/es/transparencia-y-privacidad/

Dret a la privacitat i encriptació (Outcomes FCForum2015)
http://xnet-x.net/derecho-privacidad-encriptacion/

Safe Harbor - Privacidad, Snowden y la NSA
http://www.rightsinternationalspain.org/en/blog/89/safe-harbor-privacidad-snowden-y-la-nsa


CULTURA LLIURE
Drets d'autor / difusió del coneixement online
http://2015.fcforum.net/agenda-positiva-copyright/

Agenda positiva per la reforma del copyright
http://xnet-x.net/agenda-positiva/

Carta per a la Innovació la Creativitat i l’Accés al Coneixement
http://fcforum.net/ca/charter_extended

Models Sostenibles per a la Creativitat en l’Era Digital
http://fcforum.net/ca/sustainable-models-for-creativity


A part d'això, també podeu trobar vídeos y material audiovisual cuidat que també podeu fer. En cas de que voleu subtitular algún vídeo al català també com abans se s'agrairà i reconeixerà l'autoria.

Artivisme [vídeos y +]
https://xnet-x.net/ca/eix/artivisme-i-guerrilla-de-la-comunicacio/
Cultura Lliure [vídeos y +]
https://xnet-x.net/ca/eix/cultura-lliure/
https://xnet-x.net/ca/eix/artivisme-i-guerrilla-de-la-comunicacio/

xdrudis
-----
https://donottrack-doc.com

Traque interdite, d'Arte, Upian, ONF i BR

Documentals interactius sobre privacitat, explotació de dades per
multinacionals, etc. Només està en anglès, francès i alemany. Recull
massa dades personals i és privativa (caldria obtenir permís per
traduir-la, etc.). No està pensat per projectar amb públic sinó per a
ús personal perquè és interactiu.  Per qui li agradi l'audiovisual
està bé (per efectista). Pensat pel públic en general, toca temes
interessants un pèl superficialment, però bé.



http://framasoft.org/

Associació que promou l'edició de llibres, comics, programari i
cultura en general lliures, i la "desgooglització d'internet". Posa a
disposició servidors alternatius per algunes de les funcionalitats de
les xarxes centralitzades populars. Per exemple per comptes de Google
Drive qui vulgui podia usar framadrive.org, fins que van exhaurir les
places (és una instal·lació d' OwnCloud).  Està només en francès però
si algú volgués emular-los suposo que s'ho pot traduir tot pel costum
que tenen d'usar llicències lliures.




http://www.fsf.org/resources/hw/endorsement/respects-your-freedom

Un programa de certificació de la Free Software Foundation per
acreditar que un aparell no requereix gens de programari privatiu per
funcionar i per tant respecta la llibertat de l'usuari. Com veureu
n'hi ha molt pocs i poc moderns perquè el maquinari actual fa cada cop
més difícil evitar el programari privatiu. Només en anglès. No hi és
tot el que queda decent perquè pocs fabricants demanen certificar-se,
però dóna una mica la idea de la precària situació del mercat. Per
exemple no hi ha el portàtil novena:
http://www.kosagi.com/w/index.php?title=Novena_Main_Page



http://www.replicant.us/
http://www.replicant.us/freedom-privacy-security-issues.php

Replicant és un projecte que desenvolupa una versió d'Android 100% lliure.
Passa que hi ha molts pocs mòbils que funcionin amb replicant perquè
els xips estan dissenyats per evitar que funcionin sense programari
privatiu, o simplement surten models nous més de pressa que el que la
gent pot escriure programari lliure per controlar-los sense documentació
publicada.
Només en anglès.



http://projects.goldelico.com/p/gta04-main/

GTA04. El mòbil que més s'acosta a funcionar sense programari privatiu
(exclòs el wifi, bluetooth i acceleració gràfica), i sense que els
dispositius com ara el modem tinguin accés incontrolat a la resta del
sistema. Passa que les prestacions no són gaire modernes, hi ha poca
demanda i costa finançar la producció de més unitats. El programari és
usable però no completament polit i segueixen treballant en produir
més maquinari i adaptar el programari més nou (com ara Replicant)
perquè funcioni tot bé.



https://www.fairphone.com/

Crec que on l'acta parlava de "mòbil de comerç just" volia dir aquest.
Tenen dos models, el primer usava un SOC de MediaTek, que va ser
criticat perquè l'empresa havia estat acusada (a la web, no crec que
als tribunals) per incomplir drets d'autor de programes copyleft (i
per tant no publicar codi font que haurien d'haver publicat) i el fet
que no funcionés amb programari lliure feia que els usuaris no
poguessin disposar d'actualitzacions de programari a partir de la data
que el proveïdor del SOC deixava de subministrar-lo (i podien haver-se
de canviar de mòbil i generar residus i impacte ambiental per evitar
incompatibilitats o vulnerabilitats de seguretat).  El segon model de
fairphone fa servir un SOC de Samsung i Fairphone ha pogut publicar el
codi necessari per reconstruir el programari. Això permet algunes
adaptacions i personalitzacions, i possiblement alternatives en
sistema operatiu, però com aquest codi inclou força elements privatius
sense codi font, les possibilitats d'actualització futura
(sostenibilitat) depenen de Samsung. Han millorat en el programari
però encara no han aconseguit un mòbil amb 100% de programari
lliure. El que sembla que tenen controlat és el comerç just, origen
dels materials, condicions laborals en la cadena de producció,
etc. que està molt bé i no deu ser gens fàcil.

(el SOC no és el Servei d'Ocupació de Catalunya sinó el system-on-chip, el
circuit integrat principal que aporta la major part de la funcionalitat
del mòbil).



https://libreboot.org
https://libreboot.org/faq/#intel

El projecte libreboot és una versió 100% lliure de coreboot. Volen
substituir el programari incrustat a la ROM dels PCs i portàtils que
pot comprometre tot el sistema. Bàsicament els ordinadors que es venen
actualment venen "crackejats de sèrie" i el fabricant (o els
futurs propietaris del fabricant, o el seu govern, o els atacants
que li puguin robar les claus o extorsionar-los) poden telecontrolar
l'ordinador. Per evitar que te'n puguis escapar (o també que
accedeixis a música o vídeo de formes que no els agradi als editors o
fabricants) els ordinadors actuals ja no poden funcionar exclusivament
amb programari lliure. Per això libreboot només funciona en alguns
ordinadors antics (Intel abans de 2009 i AMD abans de 2014). A la
segona URL hi ha una explicació tècnica més detallada en anglès.




http://www.defectivebydesign.org/

Una campanya de la FSF per a cridar l'atenció sobre el DRM i aquesta
mena d'abusos. El DRM és una mica l'excusa per vendre sistemes que no
pot mai acabar de controlar el propietari (n'hi ha d'altres).




http://www.fsfla.org/ikiwiki/selibre/linux-libre/index.en.html

Linux-libre. Linux libre és un projecte que es dedica a eliminar
del linux "normal" (el de kernel.org) totes les parts privatives
o que serveixen per carregar altre programari privatiu. El resultat
funciona com el linux però amb molt menys maquinari, perquè molt
maquinari necessita controladors o firmware privatiu.




http://gpl-violations.org/
https://sfconservancy.org/
http://linux-sunxi.org/GPL_Violations
https://techzei.com/developer-gears-take-mediatek-gpl-violations/

Hi ha informació en diferent grau (des de rumors a sentencies
judicials) sobre infraccions de la GPL o dels drets d'autor de
programari lliure en general. En general algunts fabricants dels SOCs
que usen els mòbils i tauletes no només no documenten públicament o
contribueixen el programari lliure necessari per utilitzar els seus
productes sinó que venen barat gràcies a agafar els programes lliures
fets per altres empreses o voluntaris i usar-los sense permís (en
general la infracció és que distribueixen programari sense el codi
font que inclou programari lliure en copyleft). Hi ha organitzacions
que intenten detectar, comunicar i negociar solucions a cada cas o
recorrer als tribunals si no hi ha manera d'arribar a un acord (i
prevenir fent difusió de bones pràctiques).  Si es pogués recopilar
una llista de casos pública i prou fiable podria ser útil per a un
consum més responsable.



http://patents.caliu.cat

Aquesta web és molt vella, si trobeu enllaços trencats pregunteu per
si puc ajudar en l'arqueologia, no sé. He vist a l'acta que parlareu
de patents. Jo vaig seguir el tema de la directiva de patents de
programari fa 10 o 15 anys però ara estic bastant desconnectat. Em
sona que a Madrid acaben d'aprovar una nova llei de patents (o estaven
a punt, no sé), que a Europa volien muntar un nou tribunal per a la
patent comunitària per a treure's de sobre la judicatura normal i
controlar la deriva de jurisprudència cap a la màxima patentibilitat
(o ja ho han fet?) i suposo que tal com estem ens podríem plantejar si
volem que la república catalana estigui a l'Oficina Europea de
Patents, a la Patent comunitària (Espanya al final s'hi va afegir o
segueix ferma pel tema de discriminació lingüística?). I suposo que el
TTIP també farà la punyeta amb patents, no sé. Jo és que ja no crec
que valgui la pena un sistema de patents en general, a aquestes
alçades, i he perdut una mica l'interés.


Documentació:
=====

Sobre Jekyll:
-----
- http://jmcglone.com/guides/github-pages/
- https://help.github.com/articles/using-jekyll-with-pages/
- http://stackoverflow.com/questions/28733425/adding-bootstrap-to-jekyll
- http://jekyllrb.com/docs/structure/
- http://jekyllrb.com/docs/datafiles/

Sobre iCal:
----
- https://github.com/Kickball/awesome-selfhosted#calendar-and-address-books
