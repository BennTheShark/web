---
layout: default
title: "Grup Promotor"
---

## El Grup Promotor del Congrés de Sobirania Tecnològica

El Congrés de Sobirania Tecnològica està organitzat pel **Grup Promotor per la Sobirania Tecnològica**. Volem que aquest grup sigui la llavor d'un espai estable per aglutinar i teixir alternatives en aquest àmbit.

Si estàs interessat o interessada a rebre informació sobre les pròximes reunions o activitats del grup promotor, o a assistir a aquestes envia un correu electrònic a: {{ site.email }}

Les reunions preparatòries del congrés es fan sempre en horari de tardes en La Lleialta Santsenca

Us animem a participar en l'organització del congrés, en la mesura que cadascú se senti més còmode: formant part del Grup Promotor, organitzant o proposant algun taller o xerrada, proposant talleristes o fent-nos arribar els vostres comentaris i propostes.
